﻿using entities.ChatClient;

namespace server_side
{
    public class Server
    {
        private readonly ServerUdpHandler UdpClient;
        private readonly ServerTcpHandler TcpClient;
        public Server()
        {
            UdpClient = new ServerUdpHandler(13000, 11000);
            TcpClient = new ServerTcpHandler(8888);
        }

        public void Start()
        {
            UdpClient.Listen();
            TcpClient.Listen();
        }

        ~Server()
        {
            UdpClient.Close();
            TcpClient.Close();
        }
    }
}
