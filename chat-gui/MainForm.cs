﻿using entities;
using entities.ChatClient;
using entities.Commands;
using entities.Helpers;
using System;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace chat_gui
{
    public partial class MainForm : Form
    {
        private ClientTcpHandler TcpClient;
        public delegate void UpdateForm();

        private Chat currentChat = null;
        public MainForm()
        {
            InitializeComponent();

            //LoginForm loginForm = new LoginForm(ConnectToServer);
            //loginForm.ShowDialog();

            ConnectToServer(NetworkHelper.GetLocalIpV4().ToString(), 8888, "Tester");

            if (TcpClient != null)
            {
                TcpClient.AddListener(UpdateInterface);
                UserCommand signInCommand = new UserCommand(CommandTypes.USER_SIGN_IN, TcpClient.User);
                TcpClient.SendCommand(signInCommand);
            }
            else
            {
                Close();
            }
        }
         
        public void ConnectToServer(string ip, int port, string username)
        {
            lblWelcome.Text = string.Format("Welcome, {0}", username);
            TcpClient = new ClientTcpHandler(port);
            TcpClient.InitUser(username);
            TcpClient.Connect();
            TcpClient.Listen();
        }

        public void UpdateInterface()
        {
            Action act = UpdateChatList;
            act += UpdateChatMessages;
            act += UpdateChatMembers;
            act.Invoke();
        }

        public void UpdateChatList()
        {
            listChats.Items.Clear();
            foreach (Chat chat in TcpClient.Chats.Items.Values)
            {
                listChats.Items.Add(ChatHelper.GetChatName(chat, TcpClient.User));
            }
        }

        public void UpdateChatMessages()
        {
            listCurrentChat.Items.Clear();
            if (currentChat != null)
            {
                lblCurrentChat.Text = "Current chat: " + ChatHelper.GetChatName(currentChat, TcpClient.User);
                currentChat = TcpClient.Chats.GetById(currentChat.Id);
                foreach (ChatMessage message in currentChat.Messages)
                {
                    listCurrentChat.Items.Add(ChatHelper.GetMessageLine(message, TcpClient.User));
                }
            }
        }

        public void UpdateChatMembers()
        {
            listMembers.Items.Clear();
            if (currentChat != null)
            {
                currentChat = TcpClient.Chats.GetById(currentChat.Id);
                foreach (User member in currentChat.Members)
                {
                    listMembers.Items.Add(ChatHelper.GetMemberLine(member, TcpClient.User));
                }
            }
        }

        private void BtnSend_Click(object sender, EventArgs e)
        {
            string message = txtMessage.Text;
            txtMessage.Text = "";
            txtMessage.Focus();
            if (currentChat == null)
            {
                MessageBox.Show("No chat was selected!");
            }
            else
            {
                ChatMessage chatMessage = new ChatMessage(TcpClient.User, message, currentChat.Id);
                if (currentChat.Name == null && currentChat.Messages.Count == 0)
                {
                    currentChat.AddMessage(chatMessage);
                    ChatCommand newChatCommand = new ChatCommand(CommandTypes.USER_CREATE_CHAT, currentChat);
                    TcpClient.SendCommand(newChatCommand);
                }
                else
                {
                    BaseCommand messageCommand = new ChatMessageCommand(CommandTypes.USER_MESSAGE, chatMessage);
                    TcpClient.SendCommand(messageCommand);
                }
            }
        }

        private void ListChats_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListBox lb = (ListBox)sender;
            int selectedIndex = lb.SelectedIndex;
            if (selectedIndex != -1)
            {
                currentChat = TcpClient.Chats.Items.Values.ToList()[selectedIndex];
                currentChat.HasUnread = false;
                lblCurrentChat.Text = ChatHelper.GetChatName(currentChat, TcpClient.User);

                UpdateChatList();
                UpdateChatMessages();
                UpdateChatMembers();
            }
        }

        private void ListMembers_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListBox lb = (ListBox)sender;
            int selectedIndex = lb.SelectedIndex;
            if (selectedIndex != -1)
            {
                User selectedMember = currentChat.Members.ToList()[selectedIndex];
                if (selectedMember.Id != TcpClient.User.Id)
                {
                    Chat chatWithUser = new Chat(selectedMember, TcpClient.User);
                    TcpClient.Chats.SetById(chatWithUser.Id, chatWithUser);
                    UpdateInterface();
                }
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            BaseCommand signOutCommand = new UserCommand(CommandTypes.USER_LOG_OUT, TcpClient.User);
            TcpClient.SendCommand(signOutCommand);
            TcpClient.Close();
        }
    }
}
