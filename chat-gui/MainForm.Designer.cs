﻿namespace chat_gui
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.listChats = new System.Windows.Forms.ListBox();
            this.listCurrentChat = new System.Windows.Forms.ListBox();
            this.listMembers = new System.Windows.Forms.ListBox();
            this.lblChats = new System.Windows.Forms.Label();
            this.lblCurrentChat = new System.Windows.Forms.Label();
            this.lblMembers = new System.Windows.Forms.Label();
            this.btnSend = new System.Windows.Forms.Button();
            this.txtMessage = new System.Windows.Forms.TextBox();
            this.lblWelcome = new System.Windows.Forms.Label();
            this.btnAttach = new System.Windows.Forms.Button();
            this.cbCurrentAttachments = new System.Windows.Forms.ComboBox();
            this.fdAddAttachment = new System.Windows.Forms.OpenFileDialog();
            this.cbMessageAttachments = new System.Windows.Forms.ComboBox();
            this.lblMessageAttachments = new System.Windows.Forms.Label();
            this.fdSaveAttachment = new System.Windows.Forms.SaveFileDialog();
            this.SuspendLayout();
            // 
            // listChats
            // 
            this.listChats.FormattingEnabled = true;
            this.listChats.ItemHeight = 16;
            this.listChats.Location = new System.Drawing.Point(3, 52);
            this.listChats.Name = "listChats";
            this.listChats.Size = new System.Drawing.Size(162, 196);
            this.listChats.TabIndex = 0;
            this.listChats.SelectedIndexChanged += new System.EventHandler(this.ListChats_SelectedIndexChanged);
            // 
            // listCurrentChat
            // 
            this.listCurrentChat.FormattingEnabled = true;
            this.listCurrentChat.ItemHeight = 16;
            this.listCurrentChat.Location = new System.Drawing.Point(171, 52);
            this.listCurrentChat.Name = "listCurrentChat";
            this.listCurrentChat.Size = new System.Drawing.Size(347, 260);
            this.listCurrentChat.TabIndex = 1;
            // 
            // listMembers
            // 
            this.listMembers.FormattingEnabled = true;
            this.listMembers.ItemHeight = 16;
            this.listMembers.Location = new System.Drawing.Point(524, 52);
            this.listMembers.Name = "listMembers";
            this.listMembers.Size = new System.Drawing.Size(162, 196);
            this.listMembers.TabIndex = 2;
            this.listMembers.SelectedIndexChanged += new System.EventHandler(this.ListMembers_SelectedIndexChanged);
            // 
            // lblChats
            // 
            this.lblChats.AutoSize = true;
            this.lblChats.Location = new System.Drawing.Point(0, 32);
            this.lblChats.Name = "lblChats";
            this.lblChats.Size = new System.Drawing.Size(44, 17);
            this.lblChats.TabIndex = 3;
            this.lblChats.Text = "Chats";
            // 
            // lblCurrentChat
            // 
            this.lblCurrentChat.AutoSize = true;
            this.lblCurrentChat.Location = new System.Drawing.Point(168, 32);
            this.lblCurrentChat.Name = "lblCurrentChat";
            this.lblCurrentChat.Size = new System.Drawing.Size(191, 17);
            this.lblCurrentChat.TabIndex = 4;
            this.lblCurrentChat.Text = "Current chat: nothing chosen";
            // 
            // lblMembers
            // 
            this.lblMembers.AutoSize = true;
            this.lblMembers.Location = new System.Drawing.Point(524, 32);
            this.lblMembers.Name = "lblMembers";
            this.lblMembers.Size = new System.Drawing.Size(99, 17);
            this.lblMembers.TabIndex = 5;
            this.lblMembers.Text = "Chat members";
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(616, 320);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(70, 26);
            this.btnSend.TabIndex = 4;
            this.btnSend.Text = "Send";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.BtnSend_Click);
            // 
            // txtMessage
            // 
            this.txtMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtMessage.Location = new System.Drawing.Point(3, 320);
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.Size = new System.Drawing.Size(607, 26);
            this.txtMessage.TabIndex = 3;
            // 
            // lblWelcome
            // 
            this.lblWelcome.AutoSize = true;
            this.lblWelcome.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblWelcome.Location = new System.Drawing.Point(-2, 7);
            this.lblWelcome.Name = "lblWelcome";
            this.lblWelcome.Size = new System.Drawing.Size(0, 25);
            this.lblWelcome.TabIndex = 8;
            // 
            // btnAttach
            // 
            this.btnAttach.Location = new System.Drawing.Point(3, 254);
            this.btnAttach.Name = "btnAttach";
            this.btnAttach.Size = new System.Drawing.Size(162, 26);
            this.btnAttach.TabIndex = 2;
            this.btnAttach.Text = "Attach file";
            this.btnAttach.UseVisualStyleBackColor = true;
            // 
            // cbCurrentAttachments
            // 
            this.cbCurrentAttachments.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbCurrentAttachments.FormattingEnabled = true;
            this.cbCurrentAttachments.Location = new System.Drawing.Point(3, 286);
            this.cbCurrentAttachments.Name = "cbCurrentAttachments";
            this.cbCurrentAttachments.Size = new System.Drawing.Size(162, 26);
            this.cbCurrentAttachments.TabIndex = 1;
            // 
            // fdAddAttachment
            // 
            this.fdAddAttachment.FileName = "openFileDialog1";
            // 
            // cbMessageAttachments
            // 
            this.cbMessageAttachments.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbMessageAttachments.FormattingEnabled = true;
            this.cbMessageAttachments.Location = new System.Drawing.Point(524, 286);
            this.cbMessageAttachments.Name = "cbMessageAttachments";
            this.cbMessageAttachments.Size = new System.Drawing.Size(162, 26);
            this.cbMessageAttachments.TabIndex = 9;
            // 
            // lblMessageAttachments
            // 
            this.lblMessageAttachments.AutoSize = true;
            this.lblMessageAttachments.Location = new System.Drawing.Point(525, 262);
            this.lblMessageAttachments.Name = "lblMessageAttachments";
            this.lblMessageAttachments.Size = new System.Drawing.Size(150, 17);
            this.lblMessageAttachments.TabIndex = 10;
            this.lblMessageAttachments.Text = "Message attachments:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(689, 348);
            this.Controls.Add(this.lblMessageAttachments);
            this.Controls.Add(this.cbMessageAttachments);
            this.Controls.Add(this.cbCurrentAttachments);
            this.Controls.Add(this.btnAttach);
            this.Controls.Add(this.lblWelcome);
            this.Controls.Add(this.txtMessage);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.lblMembers);
            this.Controls.Add(this.lblCurrentChat);
            this.Controls.Add(this.lblChats);
            this.Controls.Add(this.listMembers);
            this.Controls.Add(this.listCurrentChat);
            this.Controls.Add(this.listChats);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chat Client";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listChats;
        private System.Windows.Forms.ListBox listCurrentChat;
        private System.Windows.Forms.ListBox listMembers;
        private System.Windows.Forms.Label lblChats;
        private System.Windows.Forms.Label lblCurrentChat;
        private System.Windows.Forms.Label lblMembers;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.TextBox txtMessage;
        private System.Windows.Forms.Label lblWelcome;
        private System.Windows.Forms.Button btnAttach;
        private System.Windows.Forms.ComboBox cbCurrentAttachments;
        private System.Windows.Forms.OpenFileDialog fdAddAttachment;
        private System.Windows.Forms.ComboBox cbMessageAttachments;
        private System.Windows.Forms.Label lblMessageAttachments;
        private System.Windows.Forms.SaveFileDialog fdSaveAttachment;
    }
}

