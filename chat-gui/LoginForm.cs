﻿using entities.ChatClient;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace chat_gui
{
    public partial class LoginForm : Form
    {
        private readonly ClientUdpHandler UdpClient;
        private readonly Action<string, int, string> HandleConnectToServer;
        public LoginForm(Action<string, int, string> handleConnectToServer)
        {
            InitializeComponent();
            HandleConnectToServer = handleConnectToServer;

            UdpClient = new ClientUdpHandler(SendPort: 11000, ReceivePort: 8001);
            UdpClient.AddListener(UpdateInterface);
            UdpClient.Listen();
        }

        private void UpdateInterface()
        {
            Action act = UpdateStatus;
            act.Invoke();
        }

        private void UpdateStatus()
        {
            if (UdpClient.IsServerFound)
            {
                lblStatus.Text = "Server found";
                lblStatus.ForeColor = Color.Green;
                btnConnect.Enabled = true;
            }
            else
            {
                lblStatus.Text = "Server not found";
                lblStatus.ForeColor = Color.Red;
                btnConnect.Enabled = false;
            }
        }

        private void BtnFind_Click(object sender, EventArgs e)
        {
            UdpClient.BroadcastSearch();
        }

        private void BtnConnect_Click(object sender, EventArgs e)
        {
            if (txtNickname.Text != "")
            {
                HandleConnectToServer(UdpClient.ServerIPAddress, UdpClient.ServerPort, txtNickname.Text);
                UdpClient.Close();
                Close();
            }
            else
            {
                MessageBox.Show("Enter username");
            }
        }
    }
}
