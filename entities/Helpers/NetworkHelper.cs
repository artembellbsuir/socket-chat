﻿using System;
using System.Net;
using System.Net.Sockets;

namespace entities.Helpers
{
    public static class NetworkHelper
    {
        public static IPAddress GetLocalIpV4()
        {
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip;
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }
    }
}
