﻿using System;

namespace entities.Helpers
{
    public static class ChatHelper
    {
        public static string GetChatName(Chat Chat, User CurrentUser)
        {
            string chatName = "";
            if (Chat.Name == null)
            {
                foreach (User member in Chat.Members)
                {
                    if (member.Id != CurrentUser.Id)
                    {
                        chatName += member.Username + " (private)";
                    }
                }
            }
            else
            {
                chatName = Chat.Name;
            }

            if (Chat.HasUnread)
            {
                chatName += " UNREAD";
            }
            return chatName;
        }

        public static string GetDateString(DateTime Date)
        {
            string s = "";
            s += Date.ToString();
            return s;
        }
        public static string GetMessageLine(ChatMessage Message, User CurrentUser)
        {
            string messageLine =
                       String.Format("{0}({1}): {2}", CurrentUser.Id == Message.Sender.Id ?
                       "ME" : Message.Sender.Username, GetDateString(Message.SendingTime), Message.Text);
            return messageLine;
        }

        public static string GetMemberLine(User Member, User CurrentUser)
        {
            string memberLine = Member.Id == CurrentUser.Id ?
                        String.Format("{0} (Me)", Member.Username) : Member.Username;
            return memberLine;
        }
    }
}
