﻿using System;

namespace entities.Commands
{
    [Serializable()]
    public class ChatCommand : BaseCommand
    {
        public Chat Chat { get; }
        public ChatCommand(CommandTypes type, Chat chat) : base(type)
        {
            Chat = chat;
        }
    }
}
