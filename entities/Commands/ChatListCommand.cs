﻿using System;
using System.Collections.Generic;

namespace entities.Commands
{
    [Serializable()]
    public class ChatListCommand : BaseCommand
    {
        public List<Chat> Chats { get; }
        public ChatListCommand(CommandTypes type, List<Chat> chatList) : base(type)
        {
            Chats = chatList;
        }
    }
}
