﻿using System;

namespace entities.Commands
{
    [Serializable()]
    public class HostInfoCommand : BaseCommand
    {
        public string Ip { get; }
        public int Port { get; }
        public HostInfoCommand(CommandTypes type, string ip, int port) : base(type)
        {
            Ip = ip;
            Port = port;
        }
    }
}
