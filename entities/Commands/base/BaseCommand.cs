﻿using System;

namespace entities.Commands
{
    [Serializable()]
    public abstract class BaseCommand
    {
        public CommandTypes Type { get; }
        protected BaseCommand(CommandTypes type)
        {
            Type = type;
        }
    }
}
