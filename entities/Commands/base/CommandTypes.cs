﻿using System;

namespace entities.Commands
{
    [Serializable()]
    public enum CommandTypes
    {
        USER_SIGN_IN,
        USER_LOG_OUT,
        USER_MESSAGE,
        USER_UPDATED_CHAT,
        USER_UPDATE_ALL_CHATS,
        USER_CREATE_CHAT,

        UDP_CLIENT_INFO,
        UDP_SERVER_INFO,


        USER_UPDATE_CHAT_LIST,
    }
}
