﻿using System;

namespace entities.Commands
{
    [Serializable()]
    public class ChatMessageCommand : BaseCommand
    {
        public ChatMessage Message { get; }
        public ChatMessageCommand(CommandTypes type, ChatMessage message) : base(type)
        {
            Message = message;
        }
    }
}
