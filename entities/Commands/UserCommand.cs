﻿using System;

namespace entities.Commands
{
    [Serializable()]
    public class UserCommand : BaseCommand
    {
        public User User { get; }
        public UserCommand(CommandTypes type, User user) : base(type)
        {
            User = user;
        }
    }
}
