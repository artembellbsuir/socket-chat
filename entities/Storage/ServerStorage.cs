﻿using System.Collections.Generic;
using System.Linq;

namespace entities.Storage
{
    public class ServerStorage
    {
        private readonly ChatsStorage Chats = new ChatsStorage();
        private readonly UserStorage Users = new UserStorage();
        public ServerStorage()
        {
            Chats.AddNew(new Chat("Global Chat"));
        }

        public void AddUserToGlobalChat(User user)
        {
            Chat globalChat = Chats.FindByName("Global Chat");
            globalChat.AddUser(user);
            Users.AddNew(user);
            user.AddChatId(globalChat.Id);
        }

        public Chat GetGlobalChat()
        {
            return Chats.FindByName("Global Chat");
        }

        public List<string> GetAllUsersIds()
        {
            Chat globalChat = Chats.FindByName("Global Chat");
            return globalChat.Members.Select(member => member.Id).ToList();
        }

        public List<string> GetChatUserIds(Chat chat)
        {
            Chat globalChat = Chats.GetById(chat.Id);
            return globalChat.Members.Select(member => member.Id).ToList();
        }

        public Chat AddChatMessage(ChatMessage message)
        {
            Chat updatedChat = Chats.GetById(message.ChatId);
            updatedChat.Messages.Add(message);
            return updatedChat;
        }

        public Chat CreateNewChat(Chat chat)
        {
            List<string> chatUserIds = chat.Members.Select(member => member.Id).ToList();
            chatUserIds.ForEach(userId => { User u = Users.GetById(userId); u.AddChatId(chat.Id); });
            chat.Name = chat.Members[0].Username + " + " + chat.Members[1].Username;
            Chats.SetById(chat.Id, chat);
            return Chats.GetById(chat.Id);
        }

        public List<Chat> GetAllChatsOfUser(string id)
        {
            return Chats.GetAllChatsOfUser(id);
        }

        public void RemoveUserFromAllChats(string id)
        {
            Chats.RemoveUserFromAllChats(id);
            Users.RemoveById(id);
        }

        public Chat GetChatById(string id)
        {
            return Chats.GetById(id);
        }

        public User GetUserById(string id)
        {
            return Users.GetById(id);
        }
    }
}
