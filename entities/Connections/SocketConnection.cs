﻿using entities.Commands;
using System.Net;
using System.Net.Sockets;

namespace entities.Connections
{
    public abstract class SocketConnection : IConnection
    {
        protected Socket Socket { get; private set; }
        protected EndPoint EndPoint { get; private set; }
        public SocketConnection(Socket socket, EndPoint endPoint)
        {
            Socket = socket;
            EndPoint = endPoint;
        }
        public abstract BaseCommand ReceiveCommand();
        public abstract void SendCommand(BaseCommand Command);
        public void Close()
        {
            if (Socket != null)
            {
                Socket.Shutdown(SocketShutdown.Both);
                Socket.Close();
                Socket = null;
            }
        }
    }
}
