﻿using entities.Commands;

namespace entities.Connections
{
    public interface IConnection
    {
        void SendCommand(BaseCommand Command);
        BaseCommand ReceiveCommand();
    }
}
