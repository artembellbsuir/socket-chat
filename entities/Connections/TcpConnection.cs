﻿using entities.Commands;
using entities.Connections;
using entities.ObjectSerializer;
using System;
using System.Net;
using System.Net.Sockets;

namespace entities
{
    public class TcpConnection : SocketConnection
    {
        public TcpConnection(Socket socket, EndPoint endPoint, bool isLocal) : base(socket, endPoint)
        {
            if (isLocal)
            {
                Socket.Bind(EndPoint);
            }
        }
        public TcpConnection(Socket Socket) : base(Socket, null) { }

        public void EstablishConnection()
        {
            Socket.Listen(10);
            Console.WriteLine("Server is listening...");
        }

        public void Connect()
        {
            Socket.Connect(EndPoint);
        }

        public override void SendCommand(BaseCommand command)
        {
            byte[] cmdBytes = new byte[10000];
            cmdBytes = BinarySerializer.Serialize(command);
            Socket.Send(cmdBytes);
        }

        public override BaseCommand ReceiveCommand()
        {
            byte[] cmdBytes = new byte[10000];
            Socket.Receive(cmdBytes);
            return BinarySerializer.Deserialize<BaseCommand>(cmdBytes);
        }

        public void ConnectToServer()
        {
            //Socket = new Socket(config.GetIPAddress().AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            //Socket.Connect(config.GetIPEndPoint());
            // сервер отверг запрос на подключение по адресу ...
        }

        public TcpConnection AcceptNewClientConnection()
        {
            Console.WriteLine("Waiting for next connection on: {0}", EndPoint);
            Socket clientHandler = Socket.Accept();
            Console.WriteLine("Accepted new client socket!");

            // !!! commendted
            return new TcpConnection(clientHandler);
            //return null;
        }
    }
}
