﻿using entities.Commands;
using entities.ObjectSerializer;
using System;
using System.Net;
using System.Net.Sockets;

namespace entities.Connections
{
    public class UdpConnection : SocketConnection
    {
        public UdpConnection(Socket socket, EndPoint endPoint, bool isLocal) : base(socket, endPoint)
        {
            if (isLocal)
            {
                Socket.Bind(EndPoint);
            }
        }

        public override BaseCommand ReceiveCommand()
        {
            byte[] cmdBytes = new byte[10000];
            EndPoint socketEndPoint = EndPoint;
            Socket.ReceiveFrom(cmdBytes, ref socketEndPoint);
            return BinarySerializer.Deserialize<BaseCommand>(cmdBytes);
        }

        public override void SendCommand(BaseCommand Command)
        {
            Console.WriteLine(Command.Type);
            Socket.SendTo(BinarySerializer.Serialize(Command), EndPoint);
        }
    }
}
