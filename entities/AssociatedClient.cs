﻿using entities.Commands;

namespace entities
{
    public class AssociatedClient
    {
        public string UserId { get; private set; }
        private TcpConnection Connection { get; }
        public AssociatedClient(TcpConnection сonnection)
        {
            Connection = сonnection;
        }

        public void SendCommand(BaseCommand command)
        {
            Connection.SendCommand(command);
        }

        public BaseCommand ReceiveCommand()
        {
            return Connection.ReceiveCommand();
        }

        public void InitializeUser(string userId)
        {
            UserId = userId;
        }

        public void CloseConnection()
        {
            Connection.Close();
        }
    }
}
