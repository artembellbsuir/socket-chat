﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace entities.FileHandling
{
    public class FileEntity
    {
        // + Size
        public string Id { get; private set; }
        public string UserDefinedName { get; private set; }
        public string Name { get => string.Format("{0}+{1}", UserDefinedName, Id); }
        public byte[] Data { get; private set; }
        public int Size { get => Data.Length; }

        public FileEntity(string userDefinedName, byte[] fileData)
        {
            Id = Guid.NewGuid().ToString("N");
            Data = fileData;
            UserDefinedName = userDefinedName;
        }
    }
}
