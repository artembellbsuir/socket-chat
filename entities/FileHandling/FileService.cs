﻿using entities.FileHandling;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace restful_file_storage.FileHandlers
{
    public class FileService
    {
        

        public static byte[] ConvertToByteArray(Stream stream)
        {
            byte[] buffer;
            using (MemoryStream ms = new MemoryStream())
            {
                stream.CopyTo(ms);
                buffer = ms.ToArray();
            }
            return buffer;
        }

        public static void WriteBytesToResponseBody(byte[] bytes, HttpListenerResponse response)
        {
            response.ContentLength64 = bytes.Length;
            Stream output = response.OutputStream;
            output.Write(bytes, 0, bytes.Length);
            output.Close();
        }
    }
}
