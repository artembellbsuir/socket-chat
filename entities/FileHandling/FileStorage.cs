﻿using entities.Storage;
using System;
using System.Collections.Generic;
using System.Text;

namespace entities.FileHandling
{
    public class FileStorage// : IMappedStorage<FileEntity>
    {
        public Dictionary<string, FileEntity> Files { get; private set; } = new Dictionary<string, FileEntity>();
        public FileStorage()
        {

        }

        public void AddFile(FileEntity fileEntity)
        {
            Files.Add(fileEntity.Id, fileEntity);
        }

        public bool HasFile(string fileId)
        {
            return Files.ContainsKey(fileId);
        }

        public FileEntity GetFile(string fileId)
        {
            return Files[fileId];
        }

        public void DeleteFile(string fileId)
        {
            Files.Remove(fileId);
        }

        //public void Add(FileEntity obj)
        //{
        //    throw new NotImplementedException();
        //}

        //public FileEntity Get(string id)
        //{
        //    throw new NotImplementedException();
        //}

        //public void Remove(string id)
        //{
        //    throw new NotImplementedException();
        //}

        //public bool Has(string id)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
