﻿using System;

namespace entities
{
    [Serializable()]
    public class ChatMessage
    {
        public string Text { get; set; } = "";
        public User Sender { get; } = null;
        public DateTime SendingTime { get; set; }
        public string ChatId { get; }
        public ChatMessage(User user, string text, string chatId)
        {
            Sender = user;
            Text = text;
            ChatId = chatId;
            SendingTime = DateTime.Now;
        }
    }
}
