﻿using entities.Commands;
using entities.Connections;
using entities.Helpers;
using System;
using System.Net;
using System.Net.Sockets;

namespace entities.ChatClient
{
    public class ServerUdpHandler : CustomUdpClient
    {
        public ServerUdpHandler(int SendPort, int ReceivePort) : base(SendPort, ReceivePort)
        {
            ListeningConnection = new UdpConnection(
                new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp) { EnableBroadcast = true },
                new IPEndPoint(IPAddress.Any, ReceivePort),
                isLocal: true
                );
        }

        public override void HandleCommands(object obj)
        {
            while (true)
            {
                BaseCommand command = ListeningConnection.ReceiveCommand();
                Console.WriteLine("Received UDP");

                HandleParticularCommand(command);
            }
        }

        public override void HandleParticularCommand(BaseCommand Command)
        {
            switch (Command.Type)
            {
                case CommandTypes.UDP_CLIENT_INFO:
                    {
                        HostInfoCommand requestCommand = (HostInfoCommand)Command;
                        Socket sendingUdpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp) { EnableBroadcast = true };
                        IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse(requestCommand.Ip), requestCommand.Port);

                        SendingConnection = new UdpConnection(sendingUdpSocket, endPoint, isLocal: false);

                        HostInfoCommand replyCommand = new HostInfoCommand(CommandTypes.UDP_SERVER_INFO, NetworkHelper.GetLocalIpV4().ToString(), 8888);

                        Console.WriteLine(replyCommand.Port);
                        SendingConnection.SendCommand(replyCommand);
                        break;
                    }
                default:
                    break;
            }
        }
    }
}
