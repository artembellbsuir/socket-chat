﻿using entities.Commands;
using entities.Helpers;
using entities.ObjectSerializer;
using entities.Storage;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Threading;

namespace entities.ChatClient
{
    public class ServerTcpHandler : CustomTcpClient
    {
        private readonly ServerStorage Storage = new ServerStorage();
        private readonly List<AssociatedClient> Clients;
        private Thread ClientHandleThread;
        private readonly List<Thread> ClientConnectionThreads = new List<Thread>();

        public ServerTcpHandler(int Port) : base(Port)
        {
            Connection = new TcpConnection(
                new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp),
                new IPEndPoint(NetworkHelper.GetLocalIpV4(), Port),
                isLocal: true
                );

            Clients = new List<AssociatedClient>();
        }

        public override void Connect()
        {
            Connection.Connect();
        }

        public override void Listen()
        {
            Connection.EstablishConnection();
            ClientHandleThread = new Thread(HandleClientConnections);
            ClientHandleThread.Start();
        }

        public void HandleClientConnections()
        {
            while (true)
            {
                TcpConnection newClientConnection = Connection.AcceptNewClientConnection();
                AssociatedClient newClient = new AssociatedClient(newClientConnection);
                Clients.Add(newClient);

                Thread clientHandlerThread = new Thread(HandleCommands);
                ClientConnectionThreads.Add(clientHandlerThread);
                clientHandlerThread.Start(newClient);
            }
        }

        public override void HandleCommands(object obj)
        {
            AssociatedClient client = (AssociatedClient)obj;

            try
            {
                while (true)
                {
                    BaseCommand command = client.ReceiveCommand();
                    Console.WriteLine("--> {0}", command.Type);
                    HandleParticularCommand(client, command);
                }
            }
            catch (SocketException socketException)
            {
                Clients.Remove(client);
                Console.WriteLine("Internal server error while messaging: {0}", socketException.Message);
            }
            catch (SerializationException disconnectException)
            {
                Console.WriteLine("Client is disconnected", disconnectException.Message); 
            }
        }

        public override void HandleParticularCommand(AssociatedClient Client, BaseCommand Command)
        {
            switch (Command.Type)
            {
                case CommandTypes.USER_SIGN_IN:
                    {
                        UserCommand cmd = (UserCommand)Command;
                        User user = cmd.User;

                        Storage.AddUserToGlobalChat(user);
                        Client.InitializeUser(user.Id);

                        Chat updatedChat = Storage.GetGlobalChat();

                        List<Chat> updatedChats = new List<Chat> { updatedChat };
                        updatedChats.ForEach(chat => chat.HasUnread = true);
                        BaseCommand updateChatsCmd = new ChatListCommand(CommandTypes.USER_UPDATE_CHAT_LIST, updatedChats);
                        NotifyAllFromChat(Storage.GetChatUserIds(updatedChat), updateChatsCmd);
                        break;
                    }
                case CommandTypes.USER_MESSAGE:
                    {
                        ChatMessageCommand chatMessageCmd = (ChatMessageCommand)Command;
                        ChatMessage message = chatMessageCmd.Message;

                        Chat updatedChat = Storage.AddChatMessage(message);

                        List<Chat> updatedChats = new List<Chat> { updatedChat };
                        updatedChats.ForEach(chat => chat.HasUnread = true);
                        BaseCommand updateChatsCmd = new ChatListCommand(CommandTypes.USER_UPDATE_CHAT_LIST, updatedChats);
                        NotifyAllFromChat(Storage.GetChatUserIds(updatedChat), updateChatsCmd);
                        break;
                    }
                case CommandTypes.USER_CREATE_CHAT:
                    {
                        ChatCommand newChatCmd = (ChatCommand)Command;
                        Chat chat = newChatCmd.Chat;

                        Chat createdChat = Storage.CreateNewChat(chat);

                        List<Chat> updatedChats = new List<Chat> { createdChat };
                        updatedChats.ForEach(c => c.HasUnread = true);
                        BaseCommand updateChatsCmd = new ChatListCommand(CommandTypes.USER_UPDATE_CHAT_LIST, updatedChats);
                        NotifyAllFromChat(Storage.GetChatUserIds(createdChat), updateChatsCmd);
                        break;
                    }
                case CommandTypes.USER_LOG_OUT:
                    {
                        UserCommand cmd = (UserCommand)Command;
                        User user = Storage.GetUserById(cmd.User.Id);

                        Storage.RemoveUserFromAllChats(user.Id);
                        Clients.Remove(Client);

                        List<string> chatMembersIds = Storage.GetAllUsersIds();

                        foreach (AssociatedClient serverClient in Clients)
                        {
                            if (serverClient.UserId != user.Id && chatMembersIds.Contains(serverClient.UserId))
                            {
                                List<Chat> updatedChats = Storage.GetAllChatsOfUser(serverClient.UserId);
                                updatedChats.ForEach(chat => chat.HasUnread = true);
                                BaseCommand updateChatsCmd = new ChatListCommand(CommandTypes.USER_UPDATE_CHAT_LIST, updatedChats);
                                serverClient.SendCommand(updateChatsCmd);
                            }
                        }
                        break;
                    }
                default:
                    break;
            }
        }
        public void NotifyAllFromChat(List<string> targetIds, BaseCommand command)
        {
            foreach (AssociatedClient serverClient in Clients)
            {
                if (targetIds.Contains(serverClient.UserId))
                {
                    serverClient.SendCommand(command);
                }
            }
        }

        public override void HandleParticularCommand(BaseCommand Command)
        {
            throw new NotImplementedException();
        }

        public override void Close()
        {
            Clients.ForEach(client => client.CloseConnection());
            ClientConnectionThreads.ForEach(thread => { thread.Abort(); thread.Join(); });

            Connection.Close();
            ClientHandleThread.Abort();
            ClientHandleThread.Join();
        }
    }
}
