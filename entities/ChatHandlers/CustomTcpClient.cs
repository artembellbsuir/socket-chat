﻿using entities.Commands;

namespace entities.ChatClient
{
    public abstract class CustomTcpClient : ICommandHandler
    {
        public int Port { get; private set; }
        protected TcpConnection Connection { get; set; }
        public CustomTcpClient(int port)
        {
            Port = port;
        }
        public abstract void Connect();
        public abstract void Close();
        public abstract void Listen();
        public abstract void HandleCommands(object obj);
        public abstract void HandleParticularCommand(BaseCommand Command);
        public abstract void HandleParticularCommand(AssociatedClient Client, BaseCommand Command);
    }
}
