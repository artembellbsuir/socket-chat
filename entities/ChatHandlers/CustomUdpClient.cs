﻿using entities.Commands;
using entities.Connections;
using System.Threading;

namespace entities.ChatClient
{
    public abstract class CustomUdpClient : ICommandHandler
    {
        protected int SendPort { get; private set; }
        protected int ReceivePort { get; private set; }
        protected UdpConnection SendingConnection { get; set; }
        protected UdpConnection ListeningConnection { get; set; }
        protected Thread ListenUdpThread { get; set; }

        public CustomUdpClient(int SendPort, int ReceivePort)
        {
            this.SendPort = SendPort;
            this.ReceivePort = ReceivePort;
        }

        public void Listen()
        {
            ListenUdpThread = new Thread(HandleCommands);
            ListenUdpThread.Start();
        }

        public void Close()
        {
            ListeningConnection.Close();
            SendingConnection.Close();
            ListenUdpThread.Abort();
            ListenUdpThread.Join();
        }
        public abstract void HandleCommands(object obj);
        public abstract void HandleParticularCommand(BaseCommand Command);
    }
}
