﻿using entities.Commands;
using entities.Connections;
using entities.Helpers;
using System;
using System.Net;
using System.Net.Sockets;
namespace entities.ChatClient
{
    public class ClientUdpHandler : CustomUdpClient
    {
        public int ServerPort { private set; get; }
        public string ServerIPAddress { private set; get; }
        public bool IsServerFound { private set; get; }
        public event Action NotifyListener;
        public ClientUdpHandler(int SendPort, int ReceivePort) : base(SendPort, ReceivePort)
        {
            ListeningConnection = new UdpConnection(
                new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp) { EnableBroadcast = true },
                new IPEndPoint(IPAddress.Any, ReceivePort),
                isLocal: true
                );

            SendingConnection = new UdpConnection(
                new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp) { EnableBroadcast = true },
                new IPEndPoint(IPAddress.Broadcast, SendPort),
                isLocal: false
                );
        }
        public void BroadcastSearch()
        {
            BaseCommand command = new HostInfoCommand(
                CommandTypes.UDP_CLIENT_INFO,
                NetworkHelper.GetLocalIpV4().ToString(),
                ReceivePort);

            SendingConnection.SendCommand(command);
        }
        public void AddListener(Action action)
        {
            NotifyListener += action;
        }
        public override void HandleCommands(object obj)
        {
            while (true)
            {
                BaseCommand command = ListeningConnection.ReceiveCommand();
                HandleParticularCommand(command);
            }
        }

        public override void HandleParticularCommand(BaseCommand Command)
        {
            switch (Command.Type)
            {
                case CommandTypes.UDP_SERVER_INFO:
                    {
                        HostInfoCommand hostInfoCommand = (HostInfoCommand)Command;
                        ServerPort = hostInfoCommand.Port;
                        ServerIPAddress = hostInfoCommand.Ip;
                        IsServerFound = true;
                        break;
                    }
                default:
                    break;
            }

            NotifyListener?.Invoke();
        }
    }
}
