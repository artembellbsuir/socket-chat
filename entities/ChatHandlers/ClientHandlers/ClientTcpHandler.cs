﻿using entities.Commands;
using entities.Helpers;
using entities.Storage;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace entities.ChatClient
{
    public class ClientTcpHandler : CustomTcpClient
    {
        private event Action NotifyListener;
        public string ServerIp;
        public int ServerPort;
        public User User { get; private set; }
        public ChatsStorage Chats { get; private set; }
        public Thread ListenTcpThread { get; private set; }
        public ClientTcpHandler(int Port) : base(Port)
        {
            Connection = new TcpConnection(
                new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp),
                new IPEndPoint(NetworkHelper.GetLocalIpV4(), Port),
                isLocal: false
                );

            Chats = new ChatsStorage();
        }

        public override void Connect()
        {
            Connection.Connect();
        }

        public void InitUser(string username)
        {
            User = new User(username, NetworkHelper.GetLocalIpV4().ToString());
        }

        public void AddListener(Action Action)
        {
            NotifyListener += Action;
        }

        public void SendCommand(BaseCommand Command)
        {
            Connection.SendCommand(Command);
        }

        public override void Listen()
        {
            ListenTcpThread = new Thread(HandleCommands);
            ListenTcpThread.Start();
        }

        public override void HandleCommands(object obj)
        {
            try
            {
                while (true)
                {
                    BaseCommand cmd = Connection.ReceiveCommand();
                    HandleParticularCommand(cmd);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("[ERROR]: ", e.Message);
            }
        }

        public override void HandleParticularCommand(BaseCommand Command)
        {
            switch (Command.Type)
            {
                case CommandTypes.USER_UPDATE_CHAT_LIST:
                    {
                        ChatListCommand chatListCommand = (ChatListCommand)Command;
                        List<Chat> receivedChats = chatListCommand.Chats;
                        Chats.UpdateOnlyReceived(receivedChats);
                        break;
                    }
                default:
                    break;
            }

            NotifyListener?.Invoke();
        }

        public override void Close()
        {
            Connection.Close();
            ListenTcpThread.Abort();
            ListenTcpThread.Join();
        }

        public override void HandleParticularCommand(AssociatedClient Client, BaseCommand Command)
        {
            throw new NotImplementedException();
        }
    }
}
