﻿using entities.Commands;

namespace entities.ChatClient
{
    public interface ICommandHandler
    {
        void HandleCommands(object obj);
        void HandleParticularCommand(BaseCommand Command);
    }
}
